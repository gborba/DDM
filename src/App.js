import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes, useLocation, useNavigate } from 'react-router-dom';

import Home from './components/Home';
import Calculadora from './components/Calculadora';
import Galeria from './components/Galeria';
import Api from './components/Api';
import LoginScreen from './components/LoginScreen';

const App = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [prevLocation, setPrevLocation] = useState(null);

  const PrivateRoute = ({ component: Component, ...rest }) => {
    const location = useLocation();
    const navigate = useNavigate();

    if (isAuthenticated) {
      return <Component {...rest} />;
    } else {
      setPrevLocation(location.pathname);
      return <LoginScreen setIsAuthenticated={() => setIsAuthenticated(true)} />;
    }
  };

  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/calculadora" element={<Calculadora />} />
        <Route path="/galeria" element={<Galeria />} />
        <Route path="/api" element={<PrivateRoute component={Api} />} />
        <Route path="*" element={<Home />} />
      </Routes>
    </Router>
  );
};

export default App;
