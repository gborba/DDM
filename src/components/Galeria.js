import React, { useRef } from 'react';
import { View, FlatList, Image, TouchableOpacity, StyleSheet } from 'react-native';

const imagens = [
  {
    id: '1',
    uri: require('../resources/imagem1.jpg'),
    width: 100,
    height: 200,
  },
  {
    id: '2',
    uri: require('../resources/imagem2.jpg'),
    width: 150,
    height: 150,
  },
  {
    id: '3',
    uri: require('../resources/imagem3.jpg'),
    width: 200,
    height: 100,
  },
];

function Galeria() {
  const flatListRef = useRef(null);

  const renderItem = ({ item, index }) => (
    <TouchableOpacity onPress={() => flatListRef.current.scrollToIndex({ index })}>
      <View style={{ flex: 1 }}>
        <Image source={item.uri} style={{ width: item.width, height: item.height }} />
      </View>
    </TouchableOpacity>
  );

  return (
    <View style={styles.container}>
      <FlatList
        ref={flatListRef}
        horizontal
        data={imagens}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Galeria;
