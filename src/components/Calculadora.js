import React, { useState } from 'react';

function Calculadora() {
  const [notas, setNotas] = useState({ nota1: "", nota2: "" });
  const [media, setMedia] = useState(0);
  const [nome, setNome] = useState("");
  const [id, setId] = useState("");
  const [databaseData, setDatabaseData] = useState([]);

  const handleAddNota = (e) => {
    const { name, value } = e.target;
    setNotas({ ...notas, [name]: value });
  };

  const handleAddNome = (e) => {
    setNome(e.target.value);
  };

  const handleAddId = (e) => {
    setId(e.target.value);
  };

  const handleCalcularMedia = () => {
    const nota1 = Number(notas.nota1);
    const nota2 = Number(notas.nota2);
    const calculatedMedia = (nota1 + nota2) / 2;
    setMedia(calculatedMedia);

    const data = {
      id,
      nome,
      media: calculatedMedia,
    };

    storeData(data);
  };

  const storeData = (data) => {
    const request = window.indexedDB.open('alunoDatabase', 1);

    request.onupgradeneeded = (event) => {
      const db = event.target.result;
      const objectStore = db.createObjectStore('alunos', { keyPath: 'id' });
      objectStore.createIndex('nome', 'nome', { unique: false });
      objectStore.createIndex('media', 'media', { unique: false });
    };

    request.onsuccess = (event) => {
      const db = event.target.result;
      const transaction = db.transaction('alunos', 'readwrite');
      const objectStore = transaction.objectStore('alunos');
      const addRequest = objectStore.add(data);

      addRequest.onsuccess = () => {
        console.log('success');
      };

      addRequest.onerror = () => {
        console.error('error');
      };

      transaction.oncomplete = () => {
        db.close();
      };
    };

    request.onerror = () => {
      console.error('start error');
    };
  };

  const retrieveData = () => {
    const request = window.indexedDB.open('alunoDatabase', 1);

    request.onsuccess = (event) => {
      const db = event.target.result;
      const transaction = db.transaction('alunos', 'readonly');
      const objectStore = transaction.objectStore('alunos');
      const getAllRequest = objectStore.getAll();

      getAllRequest.onsuccess = () => {
        const data = getAllRequest.result;
        setDatabaseData(data);
        console.log('success.');
      };

      getAllRequest.onerror = () => {
        console.error('error request');
      };

      transaction.oncomplete = () => {
        db.close();
      };
    };

    request.onerror = () => {
      console.error('start error');
    };
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <h1>Calculadora de médias</h1>
      <div>
        <input type="text" name="nome" placeholder="Digite o nome do aluno" onChange={handleAddNome} />
        <input type="number" name="nota1" placeholder="Digite a primeira nota" onChange={handleAddNota} />
        <input type="number" name="nota2" placeholder="Digite a segunda nota" onChange={handleAddNota} />
        <input type="text" name="id" placeholder="Digite o ID do aluno" onChange={handleAddId} />
        <button onClick={handleCalcularMedia}>Calcular média e salvar</button>
      </div>
      <p>A média é: {media}</p>
      <button onClick={retrieveData}>Exibir registros</button>
      <div>
        <h2>Registros:</h2>
        <ul>
          {databaseData.map((item) => (
            <li key={item.id}>
              Nome: {item.nome}, Média: {item.media}, ID: {item.id}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Calculadora;
