import React from 'react';
import { Link } from 'react-router-dom';
import '../styles.css';

function Home() {
  return (
    <div className="container">
      <Link to="/calculadora">
        <button className="button">Calculadora de Médias</button>
      </Link>
      <Link to="/galeria">
        <button className="button">Galeria de Imagens</button>
      </Link>
      <Link to="/api">
        <button className="button">API Externa</button>
      </Link>
    </div>
  );
}

export default Home;
