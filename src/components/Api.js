import React, { useState } from "react";
import axios from "axios";

const Api = () => {
  const [data, setData] = useState([]);
  const apiKey = process.env.REACT_APP_API_KEY;
  const [query, setQuery] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const fetchData = async () => {
    const response = await axios.get(
      `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${query}`
    );
    setSearchResults(response.data.results);
  };

  const handleInputChange = (event) => {
    setQuery(event.target.value);
  };

  const handleSearch = (event) => {
    event.preventDefault();
    if (query) {
      fetchData();
    }
  };

  return (
    <div>
      <form>
        <input type="text" value={query} onChange={handleInputChange} />
        <button type="submit" onClick={handleSearch}>Buscar</button>
      </form>
      {searchResults.map((movie) => (
        <div key={movie.id}>{movie.title}</div>
      ))}
    </div>
  );
};

export default Api;
